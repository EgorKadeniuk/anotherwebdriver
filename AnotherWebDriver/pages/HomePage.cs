﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnotherWebDriver.pages
{
    public class HomePage:BasePage
    {
        private readonly IWebDriver browse;

        public IWebElement email_string
        {
            get
            {
                return browse.FindElement(By.XPath("//input[contains(@type,'email')]"));
            }
        }

        public IWebElement button_next
        {
            get
            {
                return browse.FindElement(By.XPath("//span[contains(text(),'Далее')]"));
            }
        }

        public IWebElement password_string
        {
            get
            {
                return browse.FindElement(By.XPath("//input[contains(@type,'password')]"));
            }
        }

        public IWebElement changeaccount
        {
            get
            {
                return browse.FindElement(By.XPath("//div[contains(text(),'Сменить аккаунт')]"));
            }
        }

        public HomePage(IWebDriver driver) : base(driver)
        {
            this.browse = driver;
        }

        public bool IsEmailStringVisible()
        {
            return email_string.Displayed;
        }
        
        public bool IsButtonNextVisible()
        {
            return button_next.Displayed;
        }

        public bool IsPasswordStringVisible()
        {
            return password_string.Displayed;
        }

        public bool IsChangeVisible()
        {
            return changeaccount.Displayed;
        }
        public void clickToChange()
        {
            changeaccount.Click();
        }

        public void WriteInEmail(string email)
        {
            email_string.Clear();
            email_string.SendKeys(email);
        }

        public void WriteInPassword(string password)
        {
            password_string.SendKeys(password);
        }
        
        public void ClickOnNext()
        {
            button_next.Click();
        }
    }
}
