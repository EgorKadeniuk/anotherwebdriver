﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;

namespace AnotherWebDriver.pages
{
   public class BasePage
    {
        private readonly IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void implicitWait(long timetoWait) => driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timetoWait);

        public void pageload(long timetoWait) => driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(timetoWait);
    }
}
