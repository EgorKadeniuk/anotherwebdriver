﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnotherWebDriver.pages
{
    public class EmailPage:BasePage
    {
        private readonly IWebDriver browse;

        public IWebElement button_write_to_someone
        {
            get
            {
                return browse.FindElement(By.CssSelector("div.T-I.T-I-KE.L3"));
            }
        }

        public IWebElement string_whom_to_send
        {
            get
            {
                return browse.FindElement(By.XPath("//input[contains(@peoplekit-id,'BbVjBd')]"));
            }
        }

        public IWebElement string_what_to_write
        {
            get
            {
                return browse.FindElement(By.XPath("//div[contains(@class,'Am Al editable LW-avf tS-tW')]"));
            }
        }

        public IWebElement button_send
        {
            get
            {
                return browse.FindElement(By.XPath("//div[@class = 'T-I J-J5-Ji aoO v7 T-I-atl L3']"));
            }
        }

        public IWebElement whosend
        {
            get
            {
                return browse.FindElement(By.XPath("//div[@class='yW']//span[@name='Петр Василеч']"));
            }
        }

        public IWebElement spam
        {
            get
            {
                return browse.FindElement(By.XPath("//a[contains(text(),'Спам')]"));
            }
        }

        public IWebElement more
        {
            get
            {
                return browse.FindElement(By.XPath("//span[contains(text(),'Ещё')]"));
            }
        }

        private string Url = "https://accounts.google.com/AccountChooser/signinchooser?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&flowName=GlifWebSignIn&flowEntry=AccountChooser";


        public IWebElement table
        {
            get
            {
                return browse.FindElement(By.XPath("//div[@class='AD']"));
            }
        }

        public EmailPage(IWebDriver driver):base(driver)
        {
            browse = driver;
        }

        public bool IsButtonToWriteVisible()
        {
            return button_write_to_someone.Displayed;
        }
        
        public void ClickToButtonWrite()
        {
            button_write_to_someone.Click();
        }

        public bool IsTableVisible()
        {
            return table.Displayed;
        }

        public void WriteToSomeOne(string email)
        {
            string_whom_to_send.Clear();
            string_whom_to_send.SendKeys(email);
        }

        public void WriteToSomeText(string text)
        {
            string_what_to_write.Clear();
            string_what_to_write.SendKeys(text);
        }

        public void ClickToButtonSend()
        {
            button_send.Click();
        }

        public void gotoStartWindow()
        {
            browse.Navigate().GoToUrl(Url);
        }

        public void clickTospam()
        {
            spam.Click();
        }
        public bool IsvisibleWhoSend()
        {
            return whosend.Displayed;
        }

        public void clickToMore()
        {
            more.Click();
        }
    }
}
