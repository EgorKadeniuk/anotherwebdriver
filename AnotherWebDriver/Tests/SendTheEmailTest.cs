using NUnit.Framework;
using System.Threading;

namespace AnotherWebDriver.Tests
{
    public class SendTheEmailTest:BaseTest
    {
        private string our_email = "testing.project.ts@gmail.com";

        private string our_password = "ehor2003_06";

        private string whom_to_send = "testing2.project.ts@gmail.com";

        private string some_text = "dwdwqcqwdqdqdqwdq";

        [Test]
        public void SendToEmail()
        {
            GetHomePage().IsEmailStringVisible();
            GetHomePage().WriteInEmail(our_email);
            GetHomePage().IsButtonNextVisible();
            GetHomePage().ClickOnNext();
            Thread.Sleep(1000); // Нужен если будет появляться капча
            GetHomePage().pageload(1000);
            GetHomePage().IsPasswordStringVisible();
            GetHomePage().WriteInPassword(our_password);
            GetHomePage().ClickOnNext();
            GetEmailPage().pageload(1000);
            Thread.Sleep(1000);
            GetEmailPage().implicitWait(1000);
            GetEmailPage().IsButtonToWriteVisible();
            GetEmailPage().ClickToButtonWrite();
            GetEmailPage().implicitWait(1000);
            GetEmailPage().IsTableVisible();
            GetEmailPage().WriteToSomeOne(whom_to_send);
            GetEmailPage().WriteToSomeText(some_text);
            GetEmailPage().ClickToButtonSend();
            GetEmailPage().gotoStartWindow();
            GetHomePage().IsChangeVisible();
            GetHomePage().clickToChange();
            GetHomePage().WriteInEmail(whom_to_send);
            GetHomePage().ClickOnNext();
            Thread.Sleep(1000);
            GetHomePage().WriteInPassword(our_password);
            GetHomePage().ClickOnNext();
            GetEmailPage().pageload(1000);
            GetEmailPage().clickToMore();
            GetEmailPage().clickTospam();
            Assert.IsTrue(GetEmailPage().IsvisibleWhoSend());
        }


    }
}
