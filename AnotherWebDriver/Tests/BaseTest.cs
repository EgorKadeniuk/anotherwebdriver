using AnotherWebDriver.pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AnotherWebDriver
{
    public class BaseTest
    {
        private IWebDriver driver;

        private readonly string starting_url = "https://accounts.google.com/AccountChooser/signinchooser?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&flowName=GlifWebSignIn&flowEntry=AccountChooser";
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.FullScreen();
            driver.Navigate().GoToUrl(starting_url);
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }

        public HomePage GetHomePage()
        {
            return new HomePage(GetDriver());
        }

        public EmailPage GetEmailPage()
        {
            return new EmailPage(GetDriver());
        }


        [TearDownAttribute]
        public void AfterDown() => driver.Close();
    }
}